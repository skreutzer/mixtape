<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/mixtape.lang.php
 * @author Stephan Kreutzer
 * @since 2019-12-20
 */



define("LANG_PAGETITLE", "Mixtape");
define("LANG_HEADER", "Mixtape");
define("LANG_ADD", "Hinzufügen");
define("LANG_ERROR", "Es ist ein Fehler aufgetreten.");
define("LANG_LICENSE", "Lizenzierung");



?>
