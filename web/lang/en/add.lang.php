<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/add.lang.php
 * @author Stephan Kreutzer
 * @since 2019-12-24
 */



define("LANG_PAGETITLE", "Add");
define("LANG_HEADER", "Add");
define("LANG_INPUTLABEL_ARTIST", "Artist");
define("LANG_INPUTLABEL_TITLE", "Title");
define("LANG_INPUTLABEL_LINKS", "Links (separated by linebreak)");
define("LANG_INPUTLABEL_USER", "User token");
define("LANG_BUTTONCAPTION_SUBMIT", "Submit");
define("LANG_MESSAGE_SUCCESS", "Entry added successfully!");
define("LANG_LINKCAPTION_CONTINUE", "View Mixtape");
define("LANG_MESSAGE_ERROR", "An error occurred.");
define("LANG_LINKCAPTION_RETRY", "Retry");
define("LANG_LINKCAPTION_CANCEL", "Cancel");



?>
