<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/mixtape_management.inc.php
 * @author Stephan Kreutzer
 * @since 2019-12-20
 */



require_once(dirname(__FILE__)."/database.inc.php");



function GetMixtapes()
{
    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    /** @todo entrys without entry_link don't show up, but could (not as link, but textual entry.title) */

    $entries = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."entry`.`id` AS `entry_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry`.`artist` AS `entry_artist`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry`.`title` AS `entry_title`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry`.`datetime_created` AS `entry_datetime_created`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry`.`user` AS `entry_user`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_link`.`id` AS `entry_link_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_link`.`href` AS `entry_link_href`\n".
                                              "FROM `".Database::Get()->GetPrefix()."entry_link`\n".
                                              "INNER JOIN `".Database::Get()->GetPrefix()."entry` ON\n".
                                              "    `".Database::Get()->GetPrefix()."entry`.`id` =\n".
                                              "    `".Database::Get()->GetPrefix()."entry_link`.`id_entry`\n".
                                              "WHERE 1\n".
                                              "ORDER BY `".Database::Get()->GetPrefix()."entry`.`datetime_created` DESC");

    if (is_array($entries) !== true)
    {
        return -2;
    }

    return $entries;
}

function InsertNewEntry($artist, $title, $links, $user)
{
    /** @todo Check for empty $name, $password, $email or $role. */

    if (is_array($links) != true)
    {
        return -3;
    }

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    if (Database::Get()->BeginTransaction() !== true)
    {
        return -2;
    }

    $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry` (`id`,\n".
                                  "    `artist`,\n".
                                  "    `title`,\n".
                                  "    `datetime_created`,\n".
                                  "    `user`)\n".
                                  "VALUES (?, ?, ?, UTC_TIMESTAMP(), ?)\n",
                                  array(NULL, $artist, $title, $user),
                                  array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING));

    if ($id <= 0)
    {
        Database::Get()->RollbackTransaction();
        return -4;
    }

    for ($i = 0, $max = count($links); $i < $max; $i++)
    {
        $id2 = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry_link` (`id`,\n".
                                  "    `href`,\n".
                                  "    `id_entry`)\n".
                                  "VALUES (?, ?, ?)\n",
                                  array(NULL, $links[$i], $id),
                                  array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

        if ($id2 <= 0)
        {
            Database::Get()->RollbackTransaction();
            return -5;
        }
    }

    if (Database::Get()->CommitTransaction() === true)
    {
        return $id;
    }

    return -7;
}



?>
