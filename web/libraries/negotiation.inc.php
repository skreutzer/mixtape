<?php
/* Copyright (C) 2016-2020 Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/negotiation.inc.php
 * @author Stephan Kreutzer
 * @since 2016-09-23
 * @todo Update $/web/libraries/Negotiation from
 *     https://github.com/willdurand/Negotiation.
 */



spl_autoload_register(function($class_name) {
    $path = dirname(__FILE__)."/".$class_name.".php";
    $path = str_replace("\\", "/", $path);
    require $path;
});


define("CONTENT_TYPE_SUPPORTED_XHTML", "application/xhtml+xml; charset=UTF-8; q=0.9");
define("CONTENT_TYPE_SUPPORTED_XSPF", "application/xspf+xml; charset=UTF-8; q=0.8");

function NegotiateContentType($supportedContentTypes)
{
    $acceptHeaderSuggestion = "";

    foreach ($supportedContentTypes as $acceptHeader)
    {
        if (!empty($acceptHeaderSuggestion))
        {
            $acceptHeaderSuggestion .= ",";
        }

        $acceptHeaderSuggestion .= $acceptHeader;
    }

    define("CONTENT_TYPE_SUPPORTED_ACCEPTHEADERSUGGESTION", $acceptHeaderSuggestion);

    $requestedContentTypes = "";

    if (isset($_GET['format']) === true)
    {
        switch ($_GET['format'])
        {
        case "xspf":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_XSPF;
            break;
        case "xhtml":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_XHTML;
            break;
        }
    }
    else
    {
        if (isset($_SERVER['HTTP_ACCEPT']) === true)
        {
            $requestedContentTypes = $_SERVER['HTTP_ACCEPT'];
        }
    }

    $mediaType = null;

    if (empty($requestedContentTypes) === false)
    {
        $negotiator = new \Negotiation\Negotiator();

        $mediaType = $negotiator->getBest($requestedContentTypes, $supportedContentTypes);

        if ($mediaType != null)
        {
            $mediaType = $mediaType->getValue();

        }
    }

    if ($mediaType == null)
    {
        http_response_code(406);
        echo CONTENT_TYPE_SUPPORTED_ACCEPTHEADERSUGGESTION;
        exit(-1);
    }

    define("CONTENT_TYPE_REQUESTED", $mediaType);
    header("Content-Type: ".CONTENT_TYPE_REQUESTED);
}

function NegotiateLanguage($acceptLanguageHeader, $languagePriorities)
{
    $negotiator = new \Negotiation\LanguageNegotiator();
    $language = $negotiator->getBest($acceptLanguageHeader, $languagePriorities);

    if ($language != null)
    {
        return $language->getType();
    }

    return null;
}



?>
