<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/add.php
 * @author Stephan Kreutzer
 * @since 2019-12-24
 */



require_once("./libraries/https.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("add"));

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n".
     "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:tsorter=\"http://www.terrill.ca/sorting\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "    <head>\n".
     "        <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "        <title>".LANG_PAGETITLE."</title>\n".
     "        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"mainstyle.css\"/>\n".
     "        <style type=\"text/css\">\n".
     "          .label\n".
     "          {\n".
     "              vertical-align: top;\n".
     "          }\n".
     "        </style>\n".
     "    </head>\n".
     "    <body>\n".
     "        <div class=\"mainbox\">\n".
     "          <div class=\"mainbox_header\">\n".
     "            <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "          </div>\n".
     "          <div class=\"mainbox_body\">\n";

if (isset($_POST['artist']) !== true ||
    isset($_POST['title']) !== true ||
    isset($_POST['links']) !== true ||
    isset($_POST['user']) !== true)
{
    echo "            <form action=\"add.php\" method=\"post\" class=\"table\">\n".
         "              <fieldset>\n".
         "                <div class=\"tr\">\n".
         "                  <div class=\"td\">\n".
         "                    <input id=\"input-artist\" type=\"text\" name=\"artist\" maxlength=\"254\" size=\"80\"/>\n".
         "                  </div>\n".
         "                  <div class=\"td label\">\n".
         "                    <label for=\"input-artist\">".LANG_INPUTLABEL_ARTIST."</label>\n".
         "                  </div>\n".
         "                </div>\n".
         "                <div class=\"tr\">\n".
         "                  <div class=\"td\">\n".
         "                    <input id=\"input-title\" type=\"text\" name=\"title\" maxlength=\"254\" size=\"80\"/>\n".
         "                  </div>\n".
         "                  <div class=\"td label\">\n".
         "                    <label for=\"input-title\">".LANG_INPUTLABEL_TITLE."</label>\n".
         "                  </div>\n".
         "                </div>\n".
         "                <div class=\"tr\">\n".
         "                  <div class=\"td\">\n".
         "                    <textarea id=\"input-links\" name=\"links\" rows=\"24\" cols=\"80\"></textarea>\n".
         "                  </div>\n".
         "                  <div class=\"td label\">\n".
         "                    <label for=\"input-links\">".LANG_INPUTLABEL_LINKS."</label>\n".
         "                  </div>\n".
         "                </div>\n".
         "                <div class=\"tr\">\n".
         "                  <div class=\"td\">\n".
         "                    <input id=\"input-user\" type=\"text\" name=\"user\" maxlength=\"254\" size=\"80\"/>\n".
         "                  </div>\n".
         "                  <div class=\"td label\">\n".
         "                    <label for=\"input-user\">".LANG_INPUTLABEL_USER."</label>\n".
         "                  </div>\n".
         "                </div>\n".
         "                <div class=\"tr\">\n".
         "                  <div class=\"td\">\n".
         "                    <input type=\"submit\" value=\"".LANG_BUTTONCAPTION_SUBMIT."\"/>\n".
         "                  </div>\n".
         "                  <div class=\"td label\">\n".
         "                  </div>\n".
         "                </div>\n".
         "              </fieldset>\n".
         "            </form>\n".
         "            <div>\n".
         "              <a href=\"mixtape.php\">".LANG_LINKCAPTION_CANCEL."</a>\n".
         "            </div>\n";
}
else
{
    require_once("./libraries/mixtape_management.inc.php");

    $links = array();

    if (strlen($_POST['links']) > 0)
    {
        $links = preg_split("/\s+/", $_POST['links'], -1, PREG_SPLIT_NO_EMPTY);
    }

    $id = InsertNewEntry($_POST['artist'], $_POST['title'], $links, $_POST['user']);

    if ($id > 0)
    {
        echo "            <p class=\"success\">\n".
             "              ".LANG_MESSAGE_SUCCESS."\n".
             "            </p>\n".
             "            <div>\n".
             "              <a href=\"mixtape.php\">".LANG_LINKCAPTION_CONTINUE."</a>\n".
             "            </div>\n";
    }
    else
    {
        echo "            <p class=\"error\">\n".
             "              ".LANG_MESSAGE_ERROR."\n".
             "            </p>\n".
             "            <div>\n".
             "              <a href=\"add.php\">".LANG_LINKCAPTION_RETRY."</a>\n".
             "              <a href=\"index.php\">".LANG_LINKCAPTION_CANCEL."</a>\n".
             "            </div>\n";
    }
}

echo "          </div>\n".
     "        </div>\n".
     "        <div class=\"footerbox\">\n".
     "        </div>\n".
     "    </body>\n".
     "</html>\n";



?>
