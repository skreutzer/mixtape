<?php
/* Copyright (C) 2014-2020  Stephan Kreutzer
 *
 * This file is part of mixtape.
 *
 * mixtape is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * mixtape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with mixtape. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/mixtape.php
 * @author Stephan Kreutzer
 * @since 2019-12-20
 */



require_once("./libraries/https.inc.php");

require_once("./libraries/mixtape_management.inc.php");
require_once("./libraries/negotiation.inc.php");

NegotiateContentType(array(CONTENT_TYPE_SUPPORTED_XHTML,
                           CONTENT_TYPE_SUPPORTED_XSPF));

$protocol = "https://";

if (HTTPS_ENABLED !== true)
{
    $protocol = "http://";
}

$baseUrl = $protocol.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']);

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

if ($_SERVER['REQUEST_METHOD'] === "GET")
{
    $entries = GetMixtapes();

    if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XSPF)
    {
        if (is_array($entries) === true)
        {
            header("Content-Disposition:attachment;filename=mixtape.xspf");

            echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
                 "<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n".
                 "  <title>Mixtape</title>\n".
                 /* https://gist.github.com/elundmark/7478159 */
                 "  <location>".$baseUrl."/mixtape.php</location>\n".
                 "  <trackList>\n";

            $max = count($entries);
            $lastEntryId = 0;
            $earliestDate = null;

            for ($i = 0; $i < $max; $i++)
            {
                /** @todo Also collect + add the alternative links. */
                if ($lastEntryId != (int)$entries[$i]['entry_id'])
                {
                    $lastEntryId = (int)$entries[$i]['entry_id'];
                }
                else
                {
                    continue;
                }

                echo "    <track>\n".
                     "      <title>".htmlspecialchars($entries[$i]['entry_title'], ENT_XHTML, "UTF-8")."</title>\n".
                     "      <creator>".htmlspecialchars($entries[$i]['entry_artist'], ENT_XHTML, "UTF-8")."</creator>\n".
                     "      <location>".htmlspecialchars($entries[$i]['entry_link_href'], ENT_XHTML | ENT_QUOTES, "UTF-8")."</location>\n".
                     "    </track>\n";

                if ($i === ($max - 1))
                {
                    $earliestDate = $entries[$i]['entry_datetime_created'];
                }
            }

            echo "  </trackList>\n";

            if ($earliestDate !== null)
            {
                echo "  <date>".$earliestDate."Z</date>\n";
            }

            echo "</playlist>\n";
        }
        else
        {
            http_response_code(500);
            exit(-1);
        }
    }
    else if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XHTML)
    {
        require_once("./libraries/languagelib.inc.php");
        require_once(getLanguageFile("mixtape"));

        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
             "<!DOCTYPE html\n".
             "    PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n".
             "    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n".
             "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:tsorter=\"http://www.terrill.ca/sorting\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
             "    <head>\n".
             "        <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
             "        <title>".LANG_PAGETITLE."</title>\n".
             "        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"mainstyle.css\"/>\n".
             "        <link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"mainstyle_print.css\"/>\n".
             "    </head>\n".
             "    <body>\n".
             "        <div class=\"mainbox\">\n".
             "          <div class=\"mainbox_header\">\n".
             "            <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
             "          </div>\n".
             "          <div class=\"mainbox_body\">\n";

        $currentYear = (int)date("Y");
        $currentMonth = (int)date("n");

        if (is_array($entries) === true)
        {
            $year = 0;
            $month = 0;
            $lastEntryId = 0;
            $overflow = false;
            $wasCurrent = false;
            $buffer = "";

            {
                $max = count($entries);

                for ($i = 0; $i < $max; $i++)
                {
                    $overflow = false;
                    $date = explode('-', $entries[$i]['entry_datetime_created']);
                    $wasCurrent = ($year == $currentYear && $month == $currentMonth);

                    if ($year != (int)$date[0])
                    {
                        $year = (int)$date[0];
                        $month = 0;
                        $overflow = true;
                    }

                    if ($month != (int)$date[1])
                    {
                        $month = (int)$date[1];
                        $overflow = true;
                    }

                    if ($i == 0 &&
                        ($year != $currentYear ||
                        $month != $currentMonth))
                    {
                        echo "            <div class=\"noprint\">\n".
                             "              <h2>".$currentYear."-".sprintf("%02d", $currentMonth)."</h2>\n".
                             "              <a href=\"".$baseUrl."/add.php\">".LANG_ADD."</a>\n".
                             "            </div>\n";
                    }

                    if ($overflow == true)
                    {
                        if (strlen($buffer) > 0)
                        {
                            echo $buffer."\n";
                            $buffer = "";
                        }

                        if ($i > 0)
                        {
                            echo "              </ol>\n";

                            if ($wasCurrent == true)
                            {
                                echo "              <div class=\"noprint\">\n".
                                     "                <a href=\"".$baseUrl."/add.php\">".LANG_ADD."</a>\n".
                                     "              </div>\n";
                            }

                            echo "            </div>\n";
                        }

                        echo "            <div>\n".
                             "              <h2>".$year."-".sprintf("%02d", $month)."</h2>\n".
                             "              <ol>\n";
                    }

                    $entryBuffer = "<li><a href=\"".htmlspecialchars($entries[$i]['entry_link_href'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\" class=\"noprint\">".htmlspecialchars($entries[$i]['entry_artist'], ENT_XHTML, "UTF-8").": ".htmlspecialchars($entries[$i]['entry_title'], ENT_XHTML, "UTF-8")."</a><span class=\"onlyprint\">".htmlspecialchars($entries[$i]['entry_artist'], ENT_XHTML, "UTF-8").": ".htmlspecialchars($entries[$i]['entry_title'], ENT_XHTML, "UTF-8")."; ".htmlspecialchars($entries[$i]['entry_link_href'], ENT_XHTML | ENT_QUOTES, "UTF-8")."</span>";

                    /** @todo Also collect + add the alternative links. */
                    if ($lastEntryId != (int)$entries[$i]['entry_id'])
                    {
                        $lastEntryId = (int)$entries[$i]['entry_id'];
                    }
                    else
                    {
                        continue;
                    }

                    $entryBuffer .= "</li>";

                    $buffer = $entryBuffer.$buffer;
                }

                if (strlen($buffer) > 0)
                {
                    echo $buffer;
                    $buffer = "";
                }

                if ($max > 0)
                {
                    echo "              </ol>\n";

                    if ($year == $currentYear &&
                        $month == $currentMonth)
                    {
                        echo "              <div class=\"noprint\">\n".
                             "                <a href=\"".$baseUrl."/add.php\">".LANG_ADD."</a>\n".
                             "              </div>\n";
                    }

                    echo "            </div>\n";
                }
                else
                {
                    echo "            <div class=\"noprint\">\n".
                         "              <h2>".$currentYear."-".sprintf("%02d", $currentMonth)."</h2>\n".
                         "              <a href=\"".$baseUrl."/add.php\">".LANG_ADD."</a>\n".
                         "            </div>\n";
                }
            }
        }
        else
        {
            echo "            <p class=\"error\">\n".
                 "              ".LANG_ERROR."\n".
                 "            </p>\n";
        }

        echo "          </div>\n".
             "        </div>\n".
             "        <div class=\"footerbox\">\n".
             "          <a href=\"".$baseUrl."/license.php\" class=\"footerbox_link\">".LANG_LICENSE."</a>\n".
             "        </div>\n".
             "    </body>\n".
             "</html>\n";
    }
    else
    {
        http_response_code(501);
        exit(-1);
    }
}
else
{
    http_response_code(405);
    exit(-1);
}



?>
